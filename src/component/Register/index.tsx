import * as React from "react";
import "./register.less";
import { Link } from "react-router-dom";
import { Form, Icon, Input, Button, Checkbox, message } from "antd";
import { WrappedFormUtils } from "antd/lib/form/Form";
import Http from "../../services/Http";
import { History } from 'history'
import { Utils } from "../../util";

interface IProps {
  form: WrappedFormUtils;
  history: History
}

class Register extends React.Component<IProps> {
  state = {
    confirmDirty: false
  };
  handleSubmit = (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();
    let _this = this;
    this.props.form.validateFields((err: any, values: any) => {
      if (!err) {
        let { username, password } = values;
        Http.userRegister(
          res => {
            if (res.data.code == 200) {
              Utils.LocalStorage.set('username', username);
              message.success("注册成功", 1, () => {
                this.props.history.replace('/camp')
              });

            } else {
              message.error(res.data.message, 1);
            }
          },
          { username, password }
        );
      }
    });
  };
  handleConfirmBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule: any, value: any, callback: any) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue("password")) {
      callback("两次密码不一致");
    } else {
      callback();
    }
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="register">
        <div className="register-wraper">
          <Form
            onSubmit={this.handleSubmit}
            className="register-form"

          >
          <div className="register-text">注册</div>
            <Form.Item hasFeedback={true}>
              {getFieldDecorator("username", {
                rules: [
                  { required: true, message: "请输入用户名" },
                  { max: 15, message: "最大长度15位" },
                  {
                    message: "允许英文/数字/下划线",
                    pattern: /[a-zA-Z0-9\_]+$/
                  }
                ]
              })(
                <Input
                  prefix={
                    <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  placeholder="用户名"
                />
              )}
            </Form.Item>
            <Form.Item hasFeedback={true}>
              {getFieldDecorator("password", {
                rules: [{ required: true, message: "密码不能为空" }]
              })(
                <Input
                  prefix={
                    <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  type="password"
                  placeholder="密码"
                />
              )}
            </Form.Item>
            <Form.Item hasFeedback={true}>
              {getFieldDecorator("confirm", {
                rules: [
                  {
                    required: true,
                    message: "请确认你的密码!"
                  },
                  {
                    validator: this.compareToFirstPassword
                  }
                ]
              })(
                <Input
                  prefix={
                    <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  type="password"
                  placeholder="请再次输入你的密码"
                  onBlur={this.handleConfirmBlur}
                />
              )}
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="register-form-button"
              >
                注册
              </Button>
              已经有密码？<Link to="/login">去登陆</Link>
            </Form.Item>
          </Form>
        </div>
      </div>
    );
  }
}

export default Form.create()(Register);
