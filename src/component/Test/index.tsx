import * as React from "react";
import { List, Radio, Button, Modal, message } from "antd";
import http from "../../services/Http";
import { History, Location } from "history";

import "./index.less";
import { Link } from "react-router-dom";

const RadioGroup = Radio.Group;

interface IProps {
  history: History;
  location: Location;
  match: any;
}

interface State {
  data: any[];
  answers: any[];
}

const initialState: State = {
  data: [],
  answers: []
};

type IState = Readonly<typeof initialState>;

class Test extends React.Component<IProps, IState> {
  readonly state: IState = initialState;

  componentDidMount() {
    let _this = this;
    let id = this.props.match.params.id;
    http.courseQuestions(function(res) {
      _this.setState({
        data: res.data.result
      });
    },id);
  }

  onSubmit = () => {
    let _this = this;
    if (this.state.answers.length === this.state.data.length) {
      http.courseQuestionsPost(function(res) {
        if (res.data.code === 200) {
          Modal.success({
            title: "你已完成作业",
            content: `你做对：${res.data.result}道题`,
            onOk() {
              _this.props.history.push("/homepage/list");
            },
          });
        }
      }, this.state.answers);
    } else {
      message.error("您还没有做完");
    }
  };

  onChange = (e: any, id: number) => {
    let answers = this.state.answers;
    let index = answers.findIndex(a => a.questionId === id);
    if (index > -1) {
      answers[index] = { questionId: id, answer: e.target.value };
    } else {
      answers.push({ questionId: id, answer: e.target.value });
    }
    this.setState({ answers });
  };

  render() {
    return (
      <>
        <div className="return">
          <Button>
            <Link to={`/homepage/learn/${this.props.match.params.id ? this.props.match.params.id : null}`}>返回</Link>
          </Button>
        </div>
      <div className="test">
        <List
          itemLayout="horizontal"
          dataSource={this.state.data}
          renderItem={(item: any, index: number) => (
            <List.Item>
              <List.Item.Meta
                title={`${index + 1} : ${item.title}`}
                description={
                  <RadioGroup onChange={e => this.onChange(e, item.id)}>
                    <Radio value={0}>错</Radio>
                    <Radio value={1}>对</Radio>
                  </RadioGroup>
                }
              />
            </List.Item>
          )}
        />
        <div className="submit">
          <Button type="primary" onClick={this.onSubmit} disabled={this.state.data.length === 0 || this.state.answers.length !== this.state.data.length}>
            提交
          </Button>
        </div>
        </div>
      </>
    );
  }
}

export default Test;
