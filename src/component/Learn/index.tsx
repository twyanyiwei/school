import * as React from "react";
import { History, Location } from "history";
import { CourseStore } from "../../store";

import "./index.less";
import { Button } from "antd";
import { Link } from "react-router-dom";
import http from "../../services/Http";

interface IProps {
  history: History;
  location: Location;
  courseStore: CourseStore;
  match: any;
}

interface State {
  lists: any[];
  data:any
}

const initialState: State = {
  lists: [],
  data:{}
};

type IState = Readonly<typeof initialState>;

class Learn extends React.Component<IProps, IState> {
  readonly state: IState = initialState;

  componentDidMount() {
    let id = this.props.match.params.id;
    let _this = this;
    http.courseController(function(res){
      _this.setState({
        data: res.data.result,
      })
    },id)
  }
  render() {
    let course = this.state.data;
    return (
      <>
        <div className="return">
          <Button>
            <Link to="/homepage/list">返回</Link>
          </Button>
        </div>
        <div className="course-header">
          <img
            src={
              course.baseUrl
                ? course.baseUrl
                : "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
            }
          />
          {/* <div className="course-text">
            <h2>{course.name}</h2>
            <p>{course.description}</p>
          </div> */}
        </div>
        <div className="course-content">
          <img src={course.content} />
          <div className="submit">
            <Button>
              <Link to={`/homepage/test/${course.id}`}>开始做题</Link>
            </Button>
          </div>
        </div>
      </>
    );
  }
}

export default Learn;
