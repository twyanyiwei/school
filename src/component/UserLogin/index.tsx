import * as React from "react";
import { Link } from "react-router-dom";
import { Form, Icon, Input, Button, message } from "antd";
import { WrappedFormUtils } from "antd/lib/form/Form";
import { History } from "history";
import http from "../../services/Http";
import { Utils } from "../../util";

import "./index.less";

interface IProps {
  form: WrappedFormUtils;
  history: History;
}

class Login extends React.Component<IProps, {}> {
  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    let _this = this;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        http.userLogin(function(res) {
          if (res.data.code === 200) {
            message.success("登录成功");
            Utils.LocalStorage.set("username", values.username);
          } else {
            message.error(res.data.message);
          }
          setTimeout(() => _this.props.history.push("/homepage/list"), 2000);
        }, values);
      }
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="register">
        <div className="register-wraper">
          <Form onSubmit={this.handleSubmit} className="login-form">
            <div className="login-text">登录</div>
            <Form.Item hasFeedback={true}>
              {getFieldDecorator("username", {
                rules: [
                  { required: true, message: "请输入用户名" },
                  { max: 15, message: "最大长度15位" },
                  {
                    message: "允许英文/数字/下划线",
                    pattern: /[a-z0-9\_]+$/
                  }
                ]
              })(
                <Input
                  prefix={
                    <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  placeholder="用户名"
                />
              )}
            </Form.Item>
            <Form.Item hasFeedback={true}>
              {getFieldDecorator("password", {
                rules: [{ required: true, message: "请输入密码" }]
              })(
                <Input
                  prefix={
                    <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  type="password"
                  placeholder="密码"
                />
              )}
            </Form.Item>
            <Form.Item className="login">
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
              >
                确认登录
              </Button>
              <Link to={"/register"}>注册</Link>
            </Form.Item>
          </Form>
        </div>
      </div>
    );
  }
}

export default Form.create()(Login);
