import * as React from "react";
import { inject, observer } from "mobx-react";
import { AppStore } from "../store";
import { Switch, Route, Redirect } from "react-router-dom";
import { History } from "history";

import Login from "../component/UserLogin";
import Regiester from "../component/Register";
import HomePage from "../component/HomePage";
import List from "../component/List";
import Learn from "../component/Learn";
import Test from "../component/Test";

import "./app.less";
import { Utils } from "../util";

interface IProps {
  appStore?: AppStore;
  history: History;
  location: Location;
}

@inject("appStore")
@observer
class App extends React.Component<IProps, {}> {
  render() {
    const name = Utils.LocalStorage.get("username");
    return (
      <Switch>
        <Route exact={true} path={"/login"} component={Login} />
        <Route exact={true} path={"/register"} component={Regiester} />
        <Route
          path="/homepage"
          render={({ history }) =>
            !name ? (
              <Redirect to={"/login"} />
            ) : (
              <HomePage>
                <Route exact={true} path={"/homepage/list"} component={List} />
                <Route
                  exact={true}
                  path={"/homepage/learn/:id"}
                  component={Learn}
                />
                <Route exact={true} path={"/homepage/test/:id"} component={Test} />
              </HomePage>
            )
          }
        />
        {this.props.location.pathname === "/" ? (
          <Redirect to={"/login"} />
        ) : !name ? (
          <Redirect to="/login" />
        ) : (
          <Redirect to="/homepage/list" />
        )}
      </Switch>
    );
  }
}

export default App;
