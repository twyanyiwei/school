import * as React from "react";
import { Card } from "antd";
import http from "../../services/Http";
import { History, Location } from "history";
import "./index.less";
import { CourseStore } from "../../store";
const img  = require('../../assets/banner.png') 
const practice = require('../../assets/practice.png') 
const study = require('../../assets/study.png') 
interface IProps {
  history: History;
  location: Location;
  courseStore: CourseStore;
}

interface State {
  lists: any[];
}

const initialState: State = {
  lists: []
};

type IState = Readonly<typeof initialState>;

class List extends React.Component<IProps, IState> {
  readonly state: IState = initialState;

  componentDidMount() {
    let _this = this;
    http.camps(function(res) {
      _this.setState({
        lists: res.data.result
      });
    });
  }

  chooseCard = (e:React.MouseEvent<HTMLDivElement, MouseEvent>,course:any) => {
    this.props.history.push(`/homepage/learn/${course.id}`);
  }

  render() {
    let lists = this.state.lists;
    return (
      <>
        <div className="banner">
          <img style={{width:'100%',marginTop:"30px"}}  className="banner-img" src={img} alt=""/>
        </div>
        {lists.map(list => (
          <div key={list.id} className="list-classify">
            <h4>{list.name}</h4>
            <>
              <div className="list-image">
                <img src={practice} />
              </div>
              <div className="list-courses">
              {list.courses.map((course: any) => (
                <Card
                  key={course.id}
                  hoverable
                  style={{ width: 200, height: 135 ,float:'left',marginRight:30}}
                  cover={
                    <img
                      alt="example"
                      src={study}
                    />
                  }
                  onClick={(event) => this.chooseCard(event,course)}
                >
                  {course.name}
                </Card>
              ))}
              </div>
            </>
          </div>
        ))}
      </>
    );
  }
}

export default List;
