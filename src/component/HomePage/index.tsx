import * as React from "react";
import "./HomePage.less";
import { Layout, Modal, Button } from "antd";
import { withRouter } from "react-router-dom";
import { History } from "history";
import { Utils } from "../../util";

const { Header, Content } = Layout;

type IProps = {
  history: History;
};
type IState = {
  visible: boolean;
};

class HomePage extends React.Component<IProps, IState> {
  state: IState = { visible: false };

  logout = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    e.preventDefault();
    Utils.LocalStorage.clear();
    this.props.history.push("login");
  };

  showModal = () => {
    this.setState({
      visible: true
    });
  };
  handleOk = () => {
    this.setState(
      {
        visible: false
      },
      () => {
        Utils.LocalStorage.remove("username");
        this.props.history.replace("/login");
      }
    );
  };
  handleCancel = () => {
    this.setState({
      visible: false
    });
  };
  render() {
    let name = Utils.LocalStorage.get("username");
    return (
      <Layout className="layout">
        <Header className="header">
          <div className="header-left">
            <img className="logo-image" src={require('../../assets/logo.png')} alt=""/>
            <div className="logo">思沃学院</div>
          </div>
          <div className="header-right">
            <span className="username">{localStorage.username}</span>
            <Button
              style={{background:'#F45E27',border:'none',fontSize:'12px'}}
              type="primary"
              onClick={this.showModal}
              shape="round"
              size={"small"}
            >
              退出
            </Button>
            <Modal
              visible={this.state.visible}
              onOk={this.handleOk}
              onCancel={this.handleCancel}
              cancelText={"取消"}
              centered={true}
            >
              <p>是否确认退出？</p>
            </Modal>
          </div>
        </Header>
        <Content style={{ padding: "0 50px" }}>{this.props.children}</Content>
      </Layout>
    );
  }
}
export default withRouter(HomePage as any);
