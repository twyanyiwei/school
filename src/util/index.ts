export module Utils {
  //首字母小写
  interface ReplaceFirst {
    FirstToLowerCase(str: string): string;
  }
  export class FirstStr implements ReplaceFirst {
    FirstToLowerCase(str: string) {
      return str.substring(0, 1).toLowerCase() + str.substring(1);
    }
  }
  export class LocalStorage {
    static storage = window.localStorage;
    static get(key: string): any {
      return this.storage.getItem(key)
    }
    static set(key: string, value: any): void {
      this.storage.setItem(key, value)
    }
    static remove(arg: string | string[]): void {
      if (typeof arg === 'string') {
        this.storage.removeItem(arg)
      } else {
        arg.map(a => {
          this.storage.removeItem(a)
        })
      }
    }
    static clear(): void {
      this.storage.clear()
    }
  }
}