import axios, { AxiosResponse } from 'axios';

export class Http {
  private result: any;

  constructor() {
    axios.defaults.baseURL = 'http://47.98.217.105:8080'
    axios.defaults.headers['Content-Type'] = 'application/json';
    axios.defaults.timeout = 20000;
  }

  // http://10.206.21.90:8080/users/login
  public async userLogin(callback: (response: AxiosResponse) => void,params:{username:string;password:string;}) {
    await axios.post('/users/login',params)
      .then(function (response) {
        callback(response)
      })
  }
  public async userRegister(callback: (response: AxiosResponse) => void,params:{username:string;password:string;}) {
    await axios.post('/users/register',params)
      .then(function (response) {
        callback(response)
      })
  }
  public async courseQuestions(callback: (response: AxiosResponse) => void,id: number) {
    await axios.get(`/course-questions/${id}`)
      .then(function (response) {
        callback(response)
      })
  }
  public async courseController(callback: (response: AxiosResponse) => void,id: number) {
    await axios.get(`/courses/${id}`)
      .then(function (response) {
        callback(response)
      })
  }
  public async courseQuestionsPost(callback: (response: AxiosResponse) => void,params:{answer:number;questionId:string;}[]) {
    await axios.post('/course-questions',params)
      .then(function (response) {
        callback(response)
      })
  }
  public async camps(callback: (response: AxiosResponse) => void) {
    await axios.get('/camps')
      .then(function (response) {
        callback(response)
      })
  }
}


const http = new Http();
export default http;