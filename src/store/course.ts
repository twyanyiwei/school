import { observable, action } from "mobx";

export class CourseStore {
  @observable public courseDetail: {
    baseUrl: string;
    campId: number;
    content: string;
    id: number;
    name: string;
  };

  public constructor() {
    this.courseDetail = {
      baseUrl: null,
      campId: null,
      content: null,
      id: null,
      name: null
    }
  }

  @action public setCourseDetail = (params: {
    baseUrl: string;
    campId: number;
    content: string;
    id: number;
    name: string;
  }) => {
    this.courseDetail = params;
  };
}
